from typing import List
import dataclasses

MODEL_NAMES = (
    "kapao_s_coco",
    "kapao_m_coco",
    "kapao_l_coco",
    "kapao_s_crowdpose",
    "kapao_m_crowdpose",
    "kapao_l_crowdpose",
)
CLASS_ID_PERSON = 1
BBOX_VECTOR_LENGTH = 4
KEYPOINT_VECTOR_LENGTH = 3
NUM_KEYPOINTS = 17


@dataclasses.dataclass
class Config:
    model_path: str
    model_name: str
    iou_threshold: float
    iou_threshold_kp: float
    confidence_threshold: float
    confidence_threshold_kp: float
    confidence_threshold_kp_person: float
    use_kp_detections: bool
    scales: List[float]
    flips: List[int]
    dataset_cfg: str
    input_resolution: List[int]
    augment: bool


def validate_config(config: Config) -> None:
    if len(config.input_resolution) != 2:
        raise ValueError("'input_resolution' must have length 2")
    if config.model_name not in MODEL_NAMES:
        raise ValueError(f"Invalid model name: got {config.model_name}, expected one of {MODEL_NAMES}.")
