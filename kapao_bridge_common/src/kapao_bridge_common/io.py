from typing import Dict, Any

import numpy as np
import sensor_msgs.msg

import kapao_bridge_common.config


def read_sensor_image(msg: sensor_msgs.msg.Image) -> np.ndarray:
    return np.frombuffer(msg.data, dtype=np.uint8).reshape(msg.height, msg.width, -1)


def dict_to_config(data: Dict[str, Any]) -> kapao_bridge_common.config.Config:
    return kapao_bridge_common.config.Config(**data)
