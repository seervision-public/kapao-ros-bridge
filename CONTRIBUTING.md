# Contributing Guide for Kapao ROS Bridge

We appreciate your interest in contributing to Kapao ROS Bridge! Here's how you can help.

## Code of Conduct

Before you contribute, please read and abide by our [Code of Conduct](CODE_OF_CONDUCT.md). We expect all contributors to respect these guidelines to ensure a welcoming community.

## How to Contribute

1. **Fork the Repository:** Click the 'Fork' button at the top right of this page to create your own copy of this repository.

2. **Clone the Repository:** Clone your forked repository to your local machine. You can use this command in your terminal:

   ```
   git clone https://gitlab.com/seervision-public/kapao-ros-bridge.git
   ```

3. **Create a New Branch:** Navigate to your cloned repository, then create a new branch where you'll do your work. Replace `branch-name` with your desired branch name:

   ```
   git checkout -b branch-name
   ```

4. **Make Your Changes:** Make the changes you want to contribute.

5. **Commit Your Changes:** Commit the changes you've made with a descriptive message.

   ```
   git commit -m "Your descriptive commit message"
   ```

6. **Push to Your Fork:** Push the changes in your branch to your forked repository on GitHub.

   ```
   git push origin branch-name
   ```

7. **Open a Pull Request:** Navigate to your forked repository on GitHub and click the 'New pull request' button. Review your changes and submit your pull request for review.

## Reporting Bugs or Suggesting Enhancements

If you've found a bug or have an idea for an enhancement, please [open an issue](https://gitlab.com/seervision-public/kapao-ros-bridge/issues/new). Be as detailed as possible so we can better understand and address your issue or idea.

## License

By contributing, you agree that your contributions will be licensed under its GPL-3.0 License.

## Questions?

If you have questions about contributing, feel free to [contact us](mailto:kapao-ros-bridge@seervision.com). Thank you for your interest in contributing to Kapao ROS Bridge!
