# KAPAO ROS Bridge Package

This package provides a ROS wrapper for [Kapao](https://github.com/wmcnally/kapao).

## Setup

1. Clone the [Kapao repository](https://github.com/wmcnally/kapao) and add it to your `PYTHONPATH`.
2. Install the required dependencies and download the trained models by following the instructions provided in the [Kapao Setup section](https://github.com/wmcnally/kapao#setup).

## Service endpoints

### Detect people `/kapao_bridge/compute`

Here is an example of how you can use the `compute` service:

```python
import numpy as np
import cv2
import rospy
import sensor_msgs.msg
import kapao_bridge_common.srv

def cv2_to_imgmsg_bgr8(cv_image: np.ndarray) -> sensor_msgs.msg.Image:
    """
    Convert an image of type uint8 to a ROS message.

    :param cv_image: Image to be converted
    :return: img_msg: Image as ROS message.
    """
    if not isinstance(cv_image, np.ndarray):
        raise TypeError('Your input type is not a numpy array')
    if cv_image.dtype != "uint8":
        raise TypeError(f"Wrong image type: got {cv_image.dtype}, expected uint8")

    img_msg = sensor_msgs.msg.Image()
    img_msg.height = cv_image.shape[0]
    img_msg.width = cv_image.shape[1]
    img_msg.encoding = "bgr8"

    if cv_image.dtype.byteorder == '>':
        img_msg.is_bigendian = True

    img_msg.data = cv_image.tostring()
    img_msg.step = len(img_msg.data) // img_msg.height

    return img_msg

compute_srv = rospy.ServiceProxy("/kapao_bridge/compute", kapao_bridge_common.srv.getDetections, persistent=True)
image = cv2.imread("your/image/path")
req = kapao_bridge_common.srv.getDetectionsRequest()
req.image = cv2_to_imgmsg_bgr8(image)
resp = compute_srv(req)
print(resp.bboxes, resp.poses, resp.scores)
```

After invoking the compute service, it returns bounding boxes (bboxes), poses, and scores. The dimensions of these return values are:

* `bboxes`: These are 4 * NUM_BBOXES in size. The "4" here represents the top-left (tl) and bottom-right (br) coordinates of the bounding boxes.
* `poses`: These are 3 * 17 * NUM_POSES in size. The "3" represents the x/y coordinates and score for each pose, and "17" is the number of keypoints per pose.
* `scores`: These are just NUM_SCORES in size, with each score corresponding to a detection.

### Shutdown the node `/kapao_bridge/shutdown`

Here is an example of how you can use the `shutdown` service:

```python
import rospy
import std_srvs.srv

shutdown_srv = rospy.ServiceProxy("/kapao_bridge/shutdown", std_srvs.srv.Trigger)
req = std_srvs.srv.TriggerRequest()
resp = shutdown_srv(req)
print(resp.success)
```

Please replace `"path/to/kapao_models"` and `"your/image/path"` with the actual paths to your Kapao models and image, respectively, in the provided examples for `compute services`. Make sure the file and model paths point to valid files on your system before running the code.

## Copyright

The Kapao ROS Bridge package is available under the GPL-3.0 License: See [LICENSE](./LICENSE.md) file for details.
