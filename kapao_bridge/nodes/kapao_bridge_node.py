#! /usr/bin/env python3
import rospy

import kapao_bridge.bridge


def main() -> None:
    kapao_bridge.bridge.Bridge()


if __name__ == '__main__':
    try:
        rospy.init_node('kapao_bridge')
        main()
        rospy.spin()
    except rospy.ROSInterruptException:
        rospy.loginfo('Shutting down.')
