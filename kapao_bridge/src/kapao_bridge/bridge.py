from typing import Optional, Tuple, Any, Mapping
import threading
import pathlib
import dataclasses

import std_srvs.srv
import sensor_msgs.msg
import yaml
import rospy

import kapao.models.yolo
import kapao_bridge_common.srv
import kapao_bridge_common.io
import kapao_bridge_common.config

import kapao_bridge.model


def load_config(config_path: pathlib.Path) -> kapao_bridge_common.config.Config:
    with config_path.open() as file:
        data = yaml.safe_load(file)
    config_dict = {
        field.name: data[field.name]
        for field in dataclasses.fields(kapao_bridge_common.config.Config)
    }
    config = kapao_bridge_common.io.dict_to_config(config_dict)
    return config


class Bridge:
    _SHUTDOWN_MSG = "Shutting down"

    def __init__(self) -> None:
        self.input_resolution: Optional[Tuple[int, int]] = None
        self.data: Optional[Mapping[str, Any]] = None
        self.augment = False
        self.model: Optional[kapao.models.yolo.Model] = None
        self.lock = threading.Lock()

        config_path = pathlib.Path(rospy.get_param("~config_path"))
        self._set_config(config_path)

        rospy.Subscriber("~image", sensor_msgs.msg.Image, self._compute_callback, queue_size=1)
        self.publisher = rospy.Publisher("~detections", kapao_bridge_common.msg.Detections, queue_size=3)
        rospy.Service("~compute", kapao_bridge_common.srv.getDetections, self._compute_srv_callback)
        rospy.Service("~shutdown", std_srvs.srv.Trigger, self._shutdown_srv_callback)

        rospy.loginfo(self)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(data={self.data}, augment={self.augment}, input_resolution={self.input_resolution})"

    def _set_config(
            self, config_path: pathlib.Path) -> kapao_bridge_common.srv.setStringResponse:
        if self.model is not None:
            raise NotImplementedError("Changing the config on the fly is currently not supported")
        config = load_config(config_path)
        kapao_bridge_common.config.validate_config(config)

        self.input_resolution = tuple(config.input_resolution)  # type: ignore [assignment]
        self.augment = config.augment
        model_path = pathlib.Path(config.model_path) / config.model_name
        self.model = kapao_bridge.model.load_model(model_path, self.input_resolution, True)  # type: ignore[arg-type]
        self.data = kapao_bridge.model.get_data_dict(config)

        rospy.loginfo(self)

        return kapao_bridge_common.srv.setStringResponse(success=True)

    def _compute_callback(
            self, img_msg: sensor_msgs.msg.Image) -> None:
        detections_msg = self._compute(img_msg)
        detections_msg.header = img_msg.header
        self.publisher.publish(detections_msg)

    def _compute_srv_callback(
            self, req: kapao_bridge_common.srv.getDetectionsRequest) -> kapao_bridge_common.srv.getDetectionsResponse:
        detections_msg = self._compute(req.image)
        return kapao_bridge_common.srv.getDetectionsResponse(success=True, detections=detections_msg)

    def _compute(
            self, img_msg: sensor_msgs.msg.Image) -> kapao_bridge_common.srv.getDetectionsResponse:
        if self.input_resolution is None or self.data is None or self.model is None:
            raise ValueError("'input_resolution', 'data' or 'model' not set")
        image = kapao_bridge_common.io.read_sensor_image(img_msg)
        bboxes, poses, scores = kapao_bridge.model.compute(image=image,
                                          model=self.model,
                                          data=self.data,
                                          input_resolution=self.input_resolution,
                                          augment=self.augment,
                                          lock=self.lock)
        return kapao_bridge_common.msg.Detections(bboxes=bboxes, poses=poses, scores=scores)

    def _shutdown_srv_callback(self, _: std_srvs.srv.TriggerRequest) -> std_srvs.srv.TriggerResponse:
        rospy.logwarn(self._SHUTDOWN_MSG)
        rospy.signal_shutdown(self._SHUTDOWN_MSG)
        return std_srvs.srv.TriggerResponse(success=True)
