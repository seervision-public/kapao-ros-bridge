from typing import Any, Tuple, List, Optional, Mapping
from types import MappingProxyType
import numpy as np
import pathlib
import pickle
import threading
import contextlib

import yaml
import torch
import kapao.models.yolo
import kapao.models.experimental
import kapao.utils
import kapao.val
import kapao.utils.augmentations

import kapao_bridge_common.config


def load_model(model_path: pathlib.Path,
               input_resolution: Tuple[int, int],
               warmup: bool = False) -> kapao.models.yolo.Model:
    device = _get_device()

    model = _load_model_from_meta_data(model_path.parent / f"{model_path.name}.pkl").float().fuse().eval()
    model.load_state_dict(torch.load(model_path.parent / f"{model_path.name}.pth", map_location=device))
    model.to(device)
    model.half()  # to float16

    if warmup:
        _run_model_once(model, device, input_resolution)

    return model


def _get_device() -> torch.device:
    device: torch.device = kapao.utils.torch_utils.select_device(0, batch_size=1)
    if device.type != "cuda":
        raise ValueError("Failed to load CUDA")
    return device


def _load_model_from_meta_data(pkl_path: pathlib.Path) -> kapao.models.yolo.Model:
    pkl_bytes = pkl_path.read_bytes()
    meta = pickle.loads(pkl_bytes)
    model = kapao.models.yolo.Model(cfg=meta["yaml"], nc=meta["nc"], num_coords=meta["num_coords"])
    for key, val in meta.items():
        setattr(model, key, val)
    return model


def _run_model_once(model: kapao.models.yolo.Model, device: torch.device, input_resolution: Tuple[int, int]) -> None:
    stride = int(model.stride.max())
    image_shape = kapao.utils.general.check_img_size(input_resolution, s=stride)
    model(torch.rand(1, 3, *image_shape).to(device).type_as(next(model.parameters())))


def get_data_dict(config: kapao_bridge_common.config.Config) -> Mapping[str, Any]:
    dataset_cfg_text = pathlib.Path(config.dataset_cfg).read_text()
    dataset_cfg = yaml.safe_load(dataset_cfg_text)
    # this dictionary is only for kapao-specific configuration for the github implementation
    # see here https://github.com/wmcnally/kapao/blob/ad507c2a00de330eb40d58cf0f4614d82d3c86b5/val.py#L317
    # please do not change except you know what you're doing

    # map -1 (i.e., no flips) to None to be consistent with kapao code
    flips = [None if f == -1 else f for f in config.flips]
    data = dict(
        imgsz=config.input_resolution,
        conf_thres=config.confidence_threshold,
        iou_thres=config.iou_threshold,
        use_kp_dets=config.use_kp_detections,
        conf_thres_kp=config.confidence_threshold_kp,
        iou_thres_kp=config.iou_threshold_kp,
        overwrite_tol=50,
        conf_thres_kp_person=config.confidence_threshold_kp_person,
        scales=config.scales,
        flips=flips,
        kp_flip=dataset_cfg["kp_flip"],
        nc=dataset_cfg["nc"],
        num_coords=dataset_cfg["num_coords"],
    )
    return MappingProxyType(data)


def _process_image(image: np.ndarray, input_resolution: Tuple[int, int]) -> np.ndarray:
    img = kapao.utils.augmentations.letterbox(image, new_shape=input_resolution, stride=64)[0]
    torch_image = torch.from_numpy(img).cuda()
    torch_image = torch.flip(torch_image, [2]).permute(2, 0, 1)
    torch_image = torch_image.half()  # uint8 to fp16/32
    torch_image /= 255.0  # 0 - 255 to 0.0 - 1.0
    if torch_image.ndimension() == 3:
        torch_image = torch_image.unsqueeze(0)
    return torch_image


def _process_output(output: torch.Tensor, torch_image: torch.Tensor, image_dims: Tuple[int, int],
                    data: Mapping[str, Any]) -> Tuple[List[np.ndarray], List[np.ndarray], List[np.float32]]:
    person_dets, kp_dets = kapao.val.run_nms(data, output)
    bboxes, poses, scores, _, _ = kapao.val.post_process_batch(data, torch_image, [], [[image_dims]], person_dets,
                                                               kp_dets)
    return bboxes, poses, scores


def compute(image: np.ndarray,
            model: kapao.models.yolo.Model,
            data: Mapping[str, Any],
            input_resolution: Tuple[int, int],
            augment: bool = False,
            lock: Optional[threading.Lock] = None) -> Tuple[np.ndarray, np.ndarray, np.float32]:
    torch_image = _process_image(image, input_resolution)
    lock_ = contextlib.nullcontext() if lock is None else lock
    with torch.no_grad(), lock_:  # type: ignore[attr-defined]
        output = model(torch_image,
                       augment=augment,
                       scales=data["scales"],
                       flips=data["flips"],
                       kp_flip=data["kp_flip"])[0]
    bboxes, poses, scores = _process_output(output, torch_image, image.shape[:2], data)
    bboxes_arr, poses_arr, scores_arr = np.array(bboxes), np.array(poses), np.array(scores)
    return bboxes_arr.flatten(), poses_arr.flatten(), scores_arr.flatten()
